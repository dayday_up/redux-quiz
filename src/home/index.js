import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import './style.less';

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {articles} = this.props;
    return (
      <div className="home">
        <ul className="article-list">
          {
            articles.map(article => {
              return (

                <li key={article.id} className="article-item">
                  <Link key={article.id} to={`/detail/${article.id}`}>{article.title}</Link>
                </li>

              )
            })
          }
          <li className="create-article">
            <Link to="/create">+</Link>
          </li>

        </ul>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  articles: state.articleReducer.articles
});

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);