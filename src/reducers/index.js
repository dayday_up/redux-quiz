import {combineReducers} from "redux";
import articleReducer from './articleReducer';

const reducers = combineReducers({
  articleReducer: articleReducer
});
export default reducers;