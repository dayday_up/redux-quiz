const initState = {
  articles: []
};

export default (state = initState, action = {}) => {
  switch (action.type) {
    case 'CREATE_ARTICLE':
      return {
        ...state,
        articles: [action.article, ...state.articles]
      };
    case 'FETCH_ARTICLES':
      return {
        ...state,
        articles: action.articles
      };
    case 'DELETE_ARTICLE':
      console.log(action.articleId);
      return {
        ...state,
        articles: state.articles.filter(article => {
          return article.id !== parseInt(action.articleId);
        })
      };
    default:
      return state
  }
};
