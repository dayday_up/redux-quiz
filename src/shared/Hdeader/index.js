import React from 'react';
import logo from '../../common/images/Note.png'
import './style.less'

const Header = (props) => {
  return(
    <header className="header">
      <img src={logo} alt="" className="logo"/>
      <h1 className="title">{props.text}</h1>
    </header>
  )
};

export default Header;