export const createArticle = (title, description) => (dispatch) => {
  const data = {
    title: title,
    description: description
  };
  fetch('http://localhost:8080/api/posts', {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(data),
  }).then(response => {
    if(response.status === 201) {
      return {};
    } else {
      return response.json();
    }
  }).then(data => {
    dispatch({
      type: 'CREATE_ARTICLE',
      article: data
    });
  })
};

export const fetchArticles = () => (dispatch) => {
  fetch('http://localhost:8080/api/posts').then(response => {
    return response.json();
  }).then(data => {
    console.log(data);
    dispatch({
      type: 'FETCH_ARTICLES',
      articles: data
    });
  })
};

export const deleteArticle = (id) => (dispatch) => {
  fetch(`http://localhost:8080/api/posts/${id}`, {
    method: 'DELETE',
    headers: {
      'content-type': 'application/json'
    }
  }).then(response => {
    if(response.status === 200) {
      return {};
    } else {
      return response.json();
    }
  }).then(() => {
    dispatch({
      type: 'DELETE_ARTICLE',
      articleId: id
    });
  })
};