import React, {Component} from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from './home';
import Detail from './detail';
import Create from './create';
import './App.less';
import {bindActionCreators} from 'redux';
import {fetchArticles} from './actions/articleActions';
import {connect} from 'react-redux';
import Header from './shared/Hdeader';

class App extends Component{
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.fetchArticles();
  }

  render() {
    return (
      <Router>
        <Header text="NOTES"/>
        <Switch>
          <Route path="/" exact component={Home}/>
          <Route path="/create" component={Create}/>
          <Route path="/detail/:id" exact component={Detail} />
        </Switch>
      </Router>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchArticles
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);