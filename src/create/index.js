import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {createArticle} from '../actions/articleActions';
import './style.less';
import {Link} from 'react-router-dom';

class Create extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: ''
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { title, description } = this.state;
    this.props.createArticle(title, description);
  }

  handleInputChange(field) {
    return (event) => {
      const value = event.target.value;
      const newState = {};
      newState[field] = value;
      this.setState(newState);
    }
  }

  render() {
    const { title, description } = this.state;
    return(
      <div className="create-article">
        <h1 className="title">创建笔记</h1>
        <form className="article-form">
          <div className="article-title">
            <label htmlFor="title">标题</label>
            <input type="text" id="title" className="title-input" value={title} onChange={this.handleInputChange("title")}/>
          </div>
          <div className="article-description">
            <label htmlFor="description">正文</label>
            <textarea name="description" id="description" className="description" value={description} onChange={this.handleInputChange("description")}/>
          </div>
          <div className="button-wrapper">
            <button className="submit" onClick={this.handleSubmit.bind(this)}>提交</button>
            <Link to="/"><button>取消</button></Link>
          </div>
        </form>
      </div>
    )
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => bindActionCreators({
  createArticle
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Create);