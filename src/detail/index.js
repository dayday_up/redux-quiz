import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Link} from 'react-router-dom';
import {deleteArticle} from '../actions/articleActions';
import './style.less';

class Detail extends React.Component {
  constructor(props) {
    super(props);
    this.filterArticleById = this.filterArticleById.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  filterArticleById() {
    const id = parseInt(this.props.match.params.id);
    const articles = this.props.articles;
    const article = articles.filter(article => {
      return article.id === id;
    });
    return article[0];
  }

  handleDelete(id) {
    return () => {
      this.props.deleteArticle(id);
      this.props.history.push('/');
    }
  }

  render() {
    const article = this.filterArticleById();
    const articles = this.props.articles;
    return (
      <div className="detail">
        <aside className="article-aside">
          <ul className="article-title-list">
            {
              articles.map(article => {
                return (
                  <Link key={article.id} to={`/detail/${article.id}`}>
                    <li className="article-title-item">{article.title}</li>
                  </Link>
                )
              })
            }
          </ul>
        </aside>
        {
          article
            ?
            <article className="article-detail">
              <h1 className="title">{article.title}</h1>
              <div className="content">{article.description}</div>
              <div className="button-wrapper">
                <button className="delete" onClick={this.handleDelete(article.id)}>删除</button>
                <Link to="/">
                  <button>返回</button>
                </Link>
              </div>
            </article>
            : ""
        }

      </div>
    )
  }
}

const mapStateToProps = state => {
  return ({
    articles: state.articleReducer.articles
  })
};

const mapDispatchToProps = dispatch => bindActionCreators({
  deleteArticle
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Detail);